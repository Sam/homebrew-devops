# DevOps Homebrew Tap
This repository contains a collection of Homebrew (aka, Brew) "formulae" that Sam uses

## Using this Tap
``` sh
brew tap sam/devops https://codeberg.org/Sam/homebrew-devops.git
```

## Installing AsCode
``` sh
brew install ascode
```

## Installing Isopod
``` sh
brew install isopod
```

## Installing Daytona
``` sh
brew install daytona
```