class Daytona < Formula
  desc "Lighter, alternative, implementation of the Vault client CLI primarily for services and containers"
  homepage "https://github.com/cruise-automation/daytona/"
  url "https://github.com/cruise-automation/daytona/archive/v1.1.4.tar.gz"
  sha256 "ed1484e4a4c19ffce659f1ceb221c8b1b6b26cf7d957f813e27859e5fff8dba2"
  license "Apache-2.0"
  head "https://github.com/cruise-automation/daytona"

  depends_on "go"   => :build
  depends_on "upx"  => :build

  def install
    ENV["GOPATH"] = HOMEBREW_CACHE/"go_cache"
    (buildpath/"src/github.com/cruise-automation/daytona").install buildpath.children
    cd "src/github.com/cruise-automation/daytona" do
      system "make", "build"
      bin.install "daytona"
    end
  end

  test do
    system "#{bin}/daytona", "--help", help
  end
end