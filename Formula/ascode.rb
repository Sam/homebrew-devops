class Ascode < Formula
  desc "Terraform Alternative Syntax"
  homepage "https://ascode.run/"
  url "https://github.com/mcuadros/ascode/archive/v1.2.0.tar.gz"
  sha256 "df739530275fbe355b94c65d9b26a8b66cf08d2cfe2bf393a20d068a6296ee54"
  license "GNU General Public License v3.0"
  head "https://github.com/mcuadros/ascode"

  depends_on "go" => :build

  def install
    ENV["GOPATH"] = HOMEBREW_CACHE/"go_cache"
    (buildpath/"src/github.com/mcuadros/ascode").install buildpath.children

    cd "src/github.com/mcuadros/ascode" do
      system "go", "build", "-o", bin/"ascode", "-tags", "extended", "main.go"
    end
  end

  test do
    system "#{bin}/ascode", "--help", help
  end
end