class Isopod < Formula
  desc "Expressive DSL framework for Kubernetes configuration"
  homepage "https://github.com/cruise-automation/isopod/"
  url "https://github.com/cruise-automation/isopod/archive/v1.4.2.tar.gz"
  sha256 "3e1614d1258a35dcdb2c4c185261cd6a1c151151f4e464296e41f37ab258763c"
  license "Apache-2.0"
  head "https://github.com/cruise-automation/isopod"

  depends_on "go" => :build

  def install
    ENV["GOPATH"] = HOMEBREW_CACHE/"go_cache"
    (buildpath/"src/github.com/cruise-automation/isopod").install buildpath.children

    cd "src/github.com/cruise-automation/isopod" do
      system "go", "build", "-o", bin/"isopod", "-tags", "extended", "main.go"
    end
  end

  test do
    system "#{bin}/isopod", "--help", help
  end
end